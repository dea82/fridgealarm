/*
 * buzz.h
 *
 *  Created on: 18 feb 2015
 *      Author: andreas
 */

#ifndef BUZZ_H_
#define BUZZ_H_

void Buzz_init(void);
void Buzz_loop(void);

#endif /* BUZZ_H_ */
